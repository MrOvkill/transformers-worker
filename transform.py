import json, os

from transformers import AutoModelForCausalLM, AutoTokenizer, TextStreamer

tok = AutoTokenizer.from_pretrained("abdgrt/Tinyllama-2-1b-miniguanaco")
model = AutoModelForCausalLM.from_pretrained("abdgrt/Tinyllama-2-1b-miniguanaco")

dat = json.load(open("_transform_config.json"))
if dat["prompt"] == "":
    dat["prompt"] = "You are an AI. Say something interesting about yourself."

if dat["max_new_tokens"] == None:
    dat["max_new_tokens"] = 128

inp = tok(dat["prompt"], return_tensors="pt")

# Despite returning the usual output, the streamer will also print the generated text to stdout.
output = model.generate(**inp, max_new_tokens=(dat["max_new_tokens"]))

response = tok.batch_decode(output, skip_special_tokens=True)

if not os.path.exists("prompts"):
    os.mkdir("prompts")

# Get the number of files in the prompts directory.
numFiles = len([name for name in os.listdir("prompts") if os.path.isfile(os.path.join("prompts", name))])

# Write the response to a file.
with open(f"prompts/prompt_{numFiles}.json", "w") as f:
    responseJson = {
        "prompt": dat["prompt"],
        "max_new_tokens": dat["max_new_tokens"],
        "response": response
    }
    f.write(json.dumps(responseJson))